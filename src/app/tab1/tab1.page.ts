import { Component, OnInit } from '@angular/core'
import { ProductItem } from '../product-item'
import { ProductService } from '../product.service'

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
})
export class Tab1Page implements OnInit {
  count = 1

  productsP: Promise<ProductItem[]>

  products: ProductItem[] = []

  constructor(public productService: ProductService) {}

  async ngOnInit() {
    this.productsP = this.productService.getProductList()
    this.products = await this.productsP
  }

  inc() {
    this.count++
    // this.products.push({
    //   id: (Math.random() * 100) >> 0,
    //   name: Math.random().toString(36).slice(2),
    //   price: Math.random() * 100,
    //   quantity: 0,
    // })
  }

  calcTotalPrice() {
    // let products = await this.productsP
    let acc = 0
    for (let product of this.products) {
      acc += product.price * product.quantity
    }
    return acc
  }

  addItem(item:ProductItem,event:number){
    console.log('adding item...',{item,event})
  }
}
