import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core'
import { ProductItem } from '../product-item'
import { ProductService } from '../product.service'

@Component({
  selector: 'product-list-item',
  templateUrl: './product-list-item.component.html',
  styleUrls: ['./product-list-item.component.scss'],
})
export class ProductListItemComponent implements OnInit {
  @Input()
  product: ProductItem

  @Output()
  add: EventEmitter<number> = new EventEmitter()

  timestamp = Date.now() % 1000

  constructor(public productService: ProductService) {}

  ngOnInit() {}

  async inc() {
    let quantity = await this.productService.addCart(this.product)
    this.add.emit(quantity)
  }

  get quantity() {
    return this.product.quantity.toLocaleString()
  }
}
