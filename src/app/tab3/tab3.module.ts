import { IonicModule } from '@ionic/angular'
import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule } from '@angular/forms'
import { Tab3Page } from './tab3.page'
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module'

import { Tab3PageRoutingModule } from './tab3-routing.module'
import { GeneralFormModule } from '../general-form/general-form.module'

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    GeneralFormModule,
    Tab3PageRoutingModule,
  ],
  declarations: [Tab3Page],
})
export class Tab3PageModule {}
