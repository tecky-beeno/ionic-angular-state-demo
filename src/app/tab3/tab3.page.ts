import { Component, Input, OnInit } from '@angular/core'
import { is_email } from '@beenotung/tslib/validate'
import { Field } from '../general-form/general-form.component'

interface User {
  username: string
  password: string
  email: string
  subject_id: string
}

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss'],
})
export class Tab3Page {
  fields: Field<User>[] = [
    {
      key: 'username',
      type: 'text',
      value: '',
      validate(value) {
        return value.length < 3
          ? 'username should be at least 3 characters'
          : ''
      },
    },
    {
      key: 'password',
      type: 'password',
      value: '',
      validate(value) {
        return value.length < 8
          ? 'password should be at least 8 characters'
          : ''
      },
    },
    {
      key: 'email',
      type: 'email',
      value: '',
      validate(value) {
        return !is_email(value) ? 'invalid email' : ''
      },
    },
    {
      key: 'subject_id',
      label: 'Subject',
      value: '',
      options: [
        { value: '1', text: 'Chin' },
        { value: '2', text: 'Eng' },
        { value: '3', text: 'Math' },
        { value: '4', text: 'Genenral' },
      ],
      validate(value) {
        return !value ? 'please select a subject' : ''
      },
    },
  ]

  constructor() {}

  get json() {
    return JSON.stringify(this.fields, null, 2)
  }

  signup(user: User) {
    console.log('signup', user)
  }
}
