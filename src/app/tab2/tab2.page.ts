import { Component, OnDestroy, OnInit } from '@angular/core'
import { Subscription, timer } from 'rxjs'
import { map } from 'rxjs/operators/map'

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss'],
})
export class Tab2Page implements OnInit, OnDestroy {
  count = 0

  timer

  clock = timer(0, 1000).pipe(map(value => value * 10))
  // sub: Subscription

  constructor() {}

  ngOnInit(): void {
    // this.sub = this.clock.subscribe(value => {
    //   console.log({ value })
    // })
    this.timer = setInterval(() => {
      this.count++
    }, 1000)
  }

  ngOnDestroy(): void {
    clearInterval(this.timer)
    // this.sub.unsubscribe()
  }

  inc() {
    this.count++
  }
}
