import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core'

export type Field<T extends object = {}> = {
  key: string & keyof T
  label?: string
  type?: string
  error?: string
  value: string
  validate: (value: string) => string
  options?: Array<{ value: string; text: string }>
}

@Component({
  selector: 'app-general-form',
  templateUrl: './general-form.component.html',
  styleUrls: ['./general-form.component.scss'],
})
export class GeneralFormComponent<T extends object> implements OnInit {
  focusField?: Field<T>

  @Input()
  fields: Field<T>[] = []

  @Input()
  submitText: string = 'Submit'

  @Output()
  submit = new EventEmitter<T>()

  constructor() {}

  getFieldLabel(field: Field<T>) {
    return field.label || field.key[0].toUpperCase() + field.key.slice(1)
  }

  ngOnInit(): void {
    for (let field of this.fields) {
      this.checkField(field)
    }
    delete this.focusField
  }

  checkField(field: Field<T>) {
    this.focusField = field
    field.error = field.validate(field.value)
  }

  isAllValid() {
    for (let field of this.fields) {
      if (field.error) {
        return false
      }
    }
    return true
  }

  getOverallError() {
    for (let field of this.fields) {
      if (field.error) {
        return field.error
      }
    }
  }

  emitSubmit() {
    let object: any = {}
    for (let field of this.fields) {
      object[field.key] = field.value
    }
    this.submit.emit(object)
  }
}
