import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { GeneralFormComponent } from './general-form.component'
import { FormsModule } from '@angular/forms'
import { IonicModule } from '@ionic/angular'

@NgModule({
  declarations: [GeneralFormComponent],
  exports: [GeneralFormComponent],
  imports: [CommonModule, FormsModule, IonicModule],
})
export class GeneralFormModule {}
