import { Injectable } from '@angular/core'
import { ProductItem } from './product-item'

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  constructor() {}

  async getProductList(): Promise<ProductItem[]> {
    // let res = await fetch('_API/products')
    // return res.json()
    await sleep(2000)
    return [
      { id: 1, name: 'apple', price: 12, quantity: 0 },
      { id: 2, name: 'banana', price: 34, quantity: 0 },
      { id: 3, name: 'cherry', price: 56, quantity: 0 },
    ]
  }

  async addCart(item: ProductItem) {
    // let res = await fetch('API_/product/' + item.id + '/cart')
    // let json = await res.json()
    // item.quantity = json.quantity
    await sleep(2000)
    item.quantity++
    return item.quantity
  }
}

function sleep(ms: number) {
  return new Promise((resolve, reject) => setTimeout(resolve, ms))
}
