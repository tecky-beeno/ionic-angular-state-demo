export interface ProductItem {
  id: number
  name: string
  price: number
  quantity: number
}
