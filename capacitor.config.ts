import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'ionic-angular-state-demo',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
